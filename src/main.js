import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Config from './config.js'
import VueFusionCharts from 'vue-fusioncharts'
import FusionCharts from 'fusioncharts'
import TimeSeries from 'fusioncharts/fusioncharts.timeseries';
// import IEcharts from 'vue-echarts-v3/src/full.js';

let environment = process.env.NODE_ENV || 'development'
let config = Config[environment]

createApp(App)
    .use(store)
    .use(router)
    .use(config)
    // .use(IEcharts)
    .use(VueFusionCharts, FusionCharts, TimeSeries)
    .mount('#app')
